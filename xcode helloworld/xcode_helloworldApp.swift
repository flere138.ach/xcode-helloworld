//
//  xcode_helloworldApp.swift
//  xcode helloworld
//
//  Created by Isaya on 2023/05/14.
//

import SwiftUI

@main
struct xcode_helloworldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
